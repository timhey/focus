# Tim’s OKRs 
I created this page to organize my OKRs. I try to keep a good mix of work and personal OKRs, this help me achieve my goals in both work and life. ⚖️
## OKRs: FY21-Q4 (Oct-Dec) 

### Results: Inprogress 

* [ ] **Work objectives** 
  * [ ] [Finalized-Growth Product FY 21 Q4 OKR](https://gitlab.com/gitlab-org/growth/product/-/issues/1625#note_443350455)
* [ ] **Personal objectives**
  * [ ] TBD

## Ideas for future OKRs:

**Work**
* Continue to iterate on the experiments you created for ci in q1 
* Shift focus to increasing seat counts on GitLab
   - https://gitlab.com/groups/gitlab-org/-/epics/2569
   - https://gitlab.com/gitlab-org/gitlab-design/-/issues/1035

**personal**
* Create a repo of all the various issue templates that you use so you don't have to hunt for them (oc's, experiments, customer reviews, data requests etc.)

## See also: 

* [Growth, Group::Expansion - Projects](https://gitlab.com/groups/gitlab-org/-/epics/2358)
* [Growth, Group::Expansion - Retrospectives](https://gitlab.com/gl-retrospectives/growth/expansion/-/boards?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=retrospective)
* [Product Direction Page for the Expansion Group](https://about.gitlab.com/direction/expansion/)
* [Engineering Page for the Expansion Group](https://about.gitlab.com/handbook/engineering/development/growth/expansion/)