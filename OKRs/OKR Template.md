# Qx OKRs: M-M 2021

* [ ] **Complete work** 
  * [ ] add something
  * [ ] add something
* [ ] **Add something**
* [ ] **Add something**

## Ideas for future OKRs:

* add something
* add something

## See also: 

* [Growth, Group::Expansion - Projects](https://gitlab.com/groups/gitlab-org/-/epics/2358)
* [Growth, Group::Expansion - Retrospectives](https://gitlab.com/gl-retrospectives/growth/expansion/-/boards?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=retrospective)
* [Product Direction Page for the Expansion Group](https://about.gitlab.com/direction/expansion/)
* [Engineering Page for the Expansion Group](https://about.gitlab.com/handbook/engineering/development/growth/expansion/)