## Overview
*   Group and section
*   DRI
*   Project status
*   Goals

## Problem
##### Personas
##### The problem JTBD 
##### Pain
##### Workarounds

## Business Case
##### Reach
##### impact
##### Confidence
##### Urgency and priority

## Solution
##### Viability (what is the V and what is the MVC)
##### Iteration
##### Differentiation (competitive advantage)

## Launch & Growth
##### Measures
##### Messaging
##### Headline
##### First paragraph
##### Customer quote
##### Go To Market

