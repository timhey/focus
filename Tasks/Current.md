# WEEK 49 (2020), Milestone [13.7](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&milestone_title=13.7&label_name[]=group%3A%3Aexpansion) - W2
## Priorty Tasks
* [ ] [Keep chipping away at 13.7](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aexpansion&milestone_title=13.7)
* [ ] Organize the roadmap and update the milestones to reflect next x releases vs 13.x so that we can work with eng and schedule accordingly
* [ ] Review stats for the team's KPI let's make sure we can tie the work we are doing back to this number
* [ ] Email updates may be a good place to do some testing. Check into this. 

## Data Reqeusts
##### [Link to open issues](https://gitlab.com/gitlab-data/analytics/-/issues?scope=all&utf8=%E2%9C%93&state=opened&author_username=timhey) 

##### Data reqeusts that need to be created

- [ ] udpate

## Unplanned tasks:

*  add impromptu items here as the arise so you can plan accordingly

## Ongoing tasks:
* [ ] Email marketing data base project 

## Comments:
* [ ] Connect with Mellissa from Access to align roadmaps and riff on new ideas. 

## See Also: 
* [Tim's current personal OKR's](https://gitlab.com/timhey/focus/-/blob/master/OKRs/current.md)
* [Tim's task archive](https://gitlab.com/timhey/focus/-/tree/master/Tasks/Archive)
* [Direction Page for Expansion](https://about.gitlab.com/direction/expansion/)
