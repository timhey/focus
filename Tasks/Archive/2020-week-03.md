# WEEK 3 (2020), Milestone [12.7](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&milestone_title=12.7&label_name[]=group%3A%3Aexpansion)

* [x] Advance designs for the [MR without pipeline experiment](https://gitlab.com/groups/gitlab-org/growth/-/epics/14)
* [x] Plan [12.8](https://gitlab.com/gitlab-org/growth/engineering/issues/5047) and ensure weights are added.  
* [x] Upate [Expansion Epic](https://gitlab.com/groups/gitlab-org/-/epics/2358)
* [x] Finish the requirements for the [Expansion Team Dashboard](https://gitlab.com/gitlab-data/analytics/issues/1430) 

## Unplanned tasks:

* [ ] nothing unplanned at the moment

## Ongoing tasks:

* [12.8 planning](https://gitlab.com/gitlab-org/growth/engineering/issues/5047) 

## Comments:

* This is the first week Im using a GitLab project to organize my efforts. So far it's great!