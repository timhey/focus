# WEEK 28 (2020), Milestone [13.2](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&milestone_title=13.2&label_name[]=group%3A%3Aexpansion) - W3
## Priorty Tasks

* [x] Prep for expansion sync on Wednesday - [notes](https://docs.google.com/document/d/1j2vFR5zZixgrrozrvPtuaoFxcrUejLcVqt3Mfviyq3A/edit) - cancled this week, DS and PC were out, using the notes to catch up async.
* [x] Connect with Hila about the contact sales form for Self-Managed - https://gitlab.com/gitlab-org/gitlab/-/issues/224665

## Experiment Tracking 
*[gitlab.org](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=growth%20experiment&label_name[]=group%3A%3Aexpansion)* | *[Growth Team Tasks](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=growth%20experiment&label_name[]=group%3A%3Aexpansion)*
* [Experiment Tracking: MR with no pipeline](https://gitlab.com/gitlab-org/gitlab/-/issues/212896)
* [Experiment Tracking: Add invite members to assignees dropdown](https://gitlab.com/gitlab-org/gitlab/-/issues/219278)
* [Experiment Tracking: GitLab.com notification dot when 'buy ci minutes' button is displayed in user settings](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/101)
* [Experiment Tracking: GitLab.com 'buy ci minutes' option in the top right drop down](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/102)
* [Experiment Tracking: Add an 'upgrade' option in the top right drop down (user settings menu) on GitLab.com](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/126)

## Data Reqeusts
##### [Link to open issues](https://gitlab.com/gitlab-data/analytics/-/issues?scope=all&utf8=%E2%9C%93&state=opened&author_username=timhey) 
   1. https://gitlab.com/gitlab-data/analytics/-/issues/5357
   1. https://gitlab.com/gitlab-data/analytics/-/issues/5247

##### Data reqeusts that need to be created
* Baseline for invite members 
* Finacial impact for [Improve self-managed account management
](https://docs.google.com/document/d/13swD3_c8uncH6ZdknzYL5epx1JIX2UstsZQndbBHH4s/edit)
   
## Unplanned tasks:

*  ~none 

## Ongoing tasks:

* ~none

## Comments:

* We're in the 3rd week of Milestone 13.2. 
* You are taking Friday off, make sure to complete your weekly tasks and next weeks plan by Thrusday
* 

## See Also: 
* [Tim's current personal OKR's](https://gitlab.com/timhey/focus/-/blob/master/OKRs/current.md)
* [Tim's task archive](https://gitlab.com/timhey/focus/-/tree/master/Tasks/Archive)
* [Direction Page for Expansion](https://about.gitlab.com/direction/expansion/)
