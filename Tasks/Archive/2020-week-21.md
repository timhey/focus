# WEEK 21 (2020), Milestone [13.1](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&milestone_title=13.1&label_name[]=group%3A%3Aexpansion)
## Priorty Tasks

* [x] Communiate 13.1 Planning tasks - [issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/129#note_338632687)
* [ ] Activate ci experiments (epics 28 & 30)
    - We have decided to update the design for 30 and continue to run 28 while the dashboard is beign developed. 
    - This dashboard should be completed early this week. Connect with Derek and Doug on status.
* [ ] Advance [copy iterations](https://gitlab.com/gitlab-org/growth/product/-/issues/1559) for the mr witn no pipe experiment
* [ ] Follow up on the growth page direction updates with Hila - [doc](https://docs.google.com/document/d/1ER6VqyHThKC4JroL5qEhA5b3U5EUwME83Af-JWxp42o/edit?ts=5ea89fdb#heading=h.z3dm4m28j322), [page](https://about.gitlab.com/direction/growth/)
    - What are we doing with these updates? 
* [ ] Circulate the user adoption journey flow with other PMs to generate ideas to increase stage to stage adoption - [user adoption journey](https://about.gitlab.com/handbook/product/growth/#user-adoption-journey)
* [ ] Advance issues in the backlog to design for Matej
* [ ] Advance issues in solution validation to planning breakdown and kick it off! 
* [ ] Review your q2 okr page in project [focus](https://gitlab.com/timhey/focus)

## Unplanned tasks:

*  ~none at this time~

## Ongoing tasks:

*  

## Comments:

* Milestone 13.1 starts this week
* You are taking Friday off and Monday is a public holiday in the States - So 4 day weekend! 
* Growth GC is on Monday - Sharing the results of MR with No Pipeline
* Next week is Product Key Meeting


