# WEEK 14 (2020), Milestone [12.10](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&milestone_title=12.10&label_name[]=group%3A%3Aexpansion)
## Priorty Tasks
* [x] Update [growth gc deck](https://docs.google.com/presentation/d/1kE5x40AR2CWGO5qduBzLwc0tBTj310568KahphOjD_s/edit?disco=AAAAJO-Kmwg&ts=5e7d32df&usp_dm=true) - get this done by Tuesday. 
   - showcase ideas: Mr with no pipeline experiment is live, UX scorecard for upgrading on .com is done. 2 additional experiments for CI are making progress
* [x] Review updated UX scorecards from Matej
   - https://gitlab.com/gitlab-org/gitlab-design/-/issues/1035
   - https://gitlab.com/gitlab-org/gitlab-design/-/issues/947
      - Create meta projects for these with an oppertunity canvas if needed. 
* [ ] Organize feedback with Jeff to close out research issue. 
   - https://gitlab.com/gitlab-org/ux-research/issues/656
* [x] Update the [Efficiency Improvements doc](https://docs.google.com/document/d/10zaXBXkX31wfc0DUsRb5mzFswe7nxUNCthUggwGQvsc/edit) doc updated
* [x] Attend the Reforge event this Tuesday

## Deep Work
* [ ] Advance this one through the development workflow get it into workflow validation (use the oppertunity canvas as a lever) [Automatically bill for newly added members in personal namespaces](https://gitlab.com/gitlab-org/customers-gitlab-com/issues/465) needs to be advanced use [this analysis](https://gitlab.com/gitlab-data/analytics/issues/3603) to get the discussion moving.

## Unplanned tasks:

*  ~none at this time~

## Ongoing tasks:

* KPI Change discussion next Monday keep chipping away at [this](https://drive.google.com/open?id=1PCyKBY6QTbgqhW8CxLk_F-gBhAVc_1CEVQvDZ6OTe8s)
* Keep a steady flow of updates to the [Efficiency Improvements doc](https://docs.google.com/document/d/10zaXBXkX31wfc0DUsRb5mzFswe7nxUNCthUggwGQvsc/edit). Ping Scott directly anytime the list the list is update so he can be prepared for the daily check-ins. The ask is for those owners to keep this list up to date with:  
   - a) what's the next thing you are working on to move this forward, and 
   - b) when do you expect to complete that next thing?  As an e-group we'll be meeting daily on this, so quick progress and steady updates will be greatly appreciated.

## Comments:

* We are in week 2 of 12.10 make sure the organize the backlog for 13.0
* Group GC is coming up on 2020-04-06 make sure to get a jump on this and get some good issues to showcase. 
* Next product key is 04-28


