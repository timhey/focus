# WEEK 6 (2020), Milestone [12.8](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&milestone_title=12.8&label_name[]=group%3A%3Aexpansion)
* [ ] Create opportunity canvas for "[Make it easier for users to invite their team members and friends to GitLab](https://gitlab.com/gitlab-org/growth/product/issues/48)"
* [x] Finalize validation for - WIP: Add a 'buy ci minutes' option in the top right drop down (user settings menu) on GitLab.com - [Link](https://gitlab.com/gitlab-org/growth/product/issues/844)
* [x] Finalize validation for - Add an 'upgrade' option in the top right drop down (user settings menu) on GitLab.com - [Link](https://gitlab.com/gitlab-org/growth/product/issues/843)
* [x] Finalize [Customer analysis of Ultimate discounting](https://gitlab.com/gitlab-data/analytics/issues/3431) - *This was carriered over from last week by our Data Team*
   - Here is the output from the [analysis](https://gitlab.com/gitlab-data/analytics/-/merge_requests/2306#note_282029089) 
* [x] Assign this issue for technical writing - [Update the existing documentation for upgrading self-managed subscriptions](https://gitlab.com/gitlab-org/growth/product/issues/288)
* [x] Update [FY20-Q4 OKRs](https://gitlab.com/timhey/focus/blob/master/OKRs/Archive/FY20-Q4.md) for Expansion 
* [x] Provide daily update in #ceo channel on net retention (if nothing to report, say that)
   - Here is the [thread](https://gitlab.slack.com/archives/C3MAZRM8W/p1580744399241400)

## Unplanned tasks:

* [ ] Remove "KPI" language for Kat. T
* [x] Keep tabs on MR with no pipeline project (currently in dev.)
* [x] Update [Growth Team Data Analysts Needs](https://docs.google.com/document/d/10CWEWSZTbcrTH_whb4gPF3LUp0WJ0Wqd5RQuXftzcQg/edit) for Hila
* [x] Respond to Emilie's q's about growth and Andrew Chen
* [x] Help Farnoosh with establishing success metrics for [peer review pilot](https://gitlab.com/gitlab-com/Product/issues/683)

## Ongoing tasks:

* 12.9 planning
* KPI Change discussion on Monday

## Comments:

* FY21-Q1 starts this week
* New Month starts as well
* Less than 3 weeks until %12.8 ends
* You should take a break soon and recharge - get this planned this week
* DStull is out after Tuesday make sure to connect prior to...
* This week feels a bit heavy on the meeting side don't forget to actually do work
* Growth [GC](https://docs.google.com/document/d/1zELoftrommhRdnEOAocE6jea6w7KngUDjTUQBlMCNAU/edit) is in 2 weeks on Presidents Day

## Recurring meetings this week

1. Growth Compliance Monthly - Monday @4PM
1. Weekly Product - Tuesday @10AM 
1. Bi-weekly with Phil - Tuesday @3PM
1. **License App Discussion** - Tuesday @6:30PM
1. Expansion team sync - Wednesday @10:30AM
1. Monthly sync with UX Jacki - Wednesday @1PM
1. Weekly with Hila - Wednesday @1PM
1. Monthly with Bartek - Wednesday @3PM
1. Bi-weekly Growth PM sync - Thursday @1PM


## Ad-hoc meetings and coffee chats for this week

* Virtual CAB - Wednesday @11AM
